/**
 * @author: Andrew Barndt
 * Class description: encapsulates the object behavior of a word in a word search, such as if
 * the word's string of characters, its direction, its starting and ending points, and if it's
 * been used or not.
 */

public class Word {

    private String name;
    private Direction direction;
    private Tile startTile, endTile;
    private boolean used = false;

    /**
     * Constructor for a word. Receives its name and the fact that it hasn't been used yet.
     * @param s the string of characters representing the word.
     */
    public Word(String s) {
        name = s;
        used = false;
    }

    /**
     * Getter method for the word's string of characters.
     * @return the word's letters/characters.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Setter method for the word's direction.
     * @param direction the direction the word is being set to go.
     */
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    /**
     * Setter method for what tile the word starts at.
     * @param t the tile the word starts at.
     */
    public void setStartOfWord(Tile t) {
        startTile = t;
    }

    /**
     * Setter method for what tile the word ends at.
     * @param t the tile the word ends at.
     */
    public void setEndOfWord(Tile t) {
        endTile = t;
    }

    /**
     * Getter method for the word's direction.
     * @return the direction the word is going in.
     */
    public Direction getDirection() {
        return this.direction;
    }

    /**
     * Getter method for the tile the word starts at.
     * @return the tile the word starts at.
     */
    public Tile getStartTile() {
        return startTile;
    }

    /**
     * Getter method for the tile the word ends at.
     * @return the tile the word ends at.
     */
    public Tile getEndTile() {
        return endTile;
    }

    /**
     * Sets the boolean holding if the word is played or not to true. Only needs to set the boolean
     * to true, as there is no circumstance where a word is removed once it's used.
     */
    public void setUsed() {
        this.used = true;
    }

    /**
     * Getter method for if the word is used.
     * @return true if the word has been placed; false otherwise.
     */
    public boolean getUsed() {
        return this.used;
    }
}