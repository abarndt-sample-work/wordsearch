/**
 * @author: Andrew Barndt
 * Class description: A JavaFX GUI for the word search, using a drag and drop approach.
 */

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.util.ArrayList;

public class WordSearchGUI extends Application {

    private String gameDocument;
    private static boolean solve;
    private int height, width;
    private Tile startingTile, endingTile;
    private ArrayList<Word> atTile = new ArrayList<>();
    private Tile[][] board;

    /**
     * Entry point of the program. Calls the launch method for JavaFX to launch the word search.
     * @param args command line arguments, if any.
     */
    public static void main (String[] args) {
        if (args.length > 0 && args[0].toUpperCase().equals("SOLVE")) {
            solve = true;
        }
        launch(args);
    }

    /**
     * The overriden start method that runs the actual game. Simply sets the title and goes
     * to the opening screen.
     * @param stage the background stage.
     */
    public void start (Stage stage) {
        stage.setTitle("New Game");
        openingScreen(stage);
    }

    /**
     * Manages the first screen the user sees, which prompts them for what kind of word search they
     * want, as well as the height and width of the word search. That information is used to create
     * the word search, and when the user hits start the current window closes and a new one will
     * appear with the actual word search.
     * @param stage the background stage.
     */
    private void openingScreen(Stage stage) {
        BorderPane pane = new BorderPane();
        GridPane grid = new GridPane();
        Label hLabel = new Label("");
        Label wLabel = new Label("");
        grid.setHgap(10);
        ComboBox<String> choiceMenu = new ComboBox<>();
        ObservableList<String> choices = FXCollections.observableArrayList("Geography", "All words");
        choiceMenu.setItems(choices);

        //As necessary, when the user enters height and width numbers, alert them if it's not
        //a valid number
        TextField enteredHeight = new TextField("");
        enteredHeight.setPromptText("Height");
        enteredHeight.addEventHandler(KeyEvent.ANY, event -> {
            if (enteredHeight.getText().equals("")) {
                hLabel.setText(null);
            } else {
                hLabel.setTextFill(Color.RED);
                for (int i = 0; i < enteredHeight.getText().length(); i++) {
                    if (enteredHeight.getText().charAt(i) < '0' || enteredHeight.getText().charAt(i) > '9') {
                        hLabel.setText("Not a valid entry");

                        //Return so if it's not a valid entry, Java doesn't try to turn it into an
                        //int and throw an exception
                        return;
                    }
                }
                int heightTest = Integer.parseInt(enteredHeight.getText());
                if (heightTest > WSConstants.MAX_HEIGHT || heightTest < WSConstants.MIN_HEIGHT) {
                    hLabel.setText("Not a valid number");
                } else hLabel.setText(null);
            }
        });

        TextField enteredWidth = new TextField("");
        enteredWidth.setPromptText("Width");
        enteredWidth.addEventHandler(KeyEvent.ANY, event -> {
            if (enteredWidth.getText().equals("")) {
                wLabel.setText(null);
            } else {
                wLabel.setTextFill(Color.RED);
                for (int i = 0; i < enteredWidth.getText().length(); i++) {
                    if (enteredWidth.getText().charAt(i) < '0' || enteredWidth.getText().charAt(i) > '9') {
                        wLabel.setText("Not a valid entry");
                        return;
                    }
                }
                int widthTest = Integer.parseInt(enteredWidth.getText());
                if (widthTest > WSConstants.MAX_WIDTH || widthTest < WSConstants.MIN_WIDTH) {
                    wLabel.setText("Not a valid number");
                } else wLabel.setText(null);
            }
        });

        Button startButton = new Button("Start");
        startButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (choiceMenu.getValue() == null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setContentText("Please select a word type.");
                alert.showAndWait();
            }
            //If the values for height and width are not numbers, do nothing when start is clicked
            else {
                for (int i = 0; i < enteredWidth.getText().length(); i++) {
                    if (enteredWidth.getText().charAt(i) < '0' ||
                            enteredWidth.getText().charAt(i) > '9') {
                        return;
                    }
                }
                for (int i = 0; i < enteredHeight.getText().length(); i++) {
                    if (enteredHeight.getText().charAt(i) < '0' ||
                            enteredHeight.getText().charAt(i) > '9') {
                        return;
                    }
                }
                if (enteredHeight.getText().equals("") || enteredWidth.getText().equals("") ||
                    enteredHeight.getText() == null || enteredWidth.getText() == null) {
                    return;
                }
                int heightTest = Integer.parseInt(enteredHeight.getText());
                int widthTest = Integer.parseInt(enteredWidth.getText());

                //If the numbers are in range, take the selected values and start the actual
                //word search
                if (widthTest <= WSConstants.MAX_WIDTH &&
                        widthTest >= WSConstants.MIN_WIDTH &&
                        heightTest >= WSConstants.MIN_HEIGHT &&
                        heightTest <= WSConstants.MAX_HEIGHT) {
                    height = heightTest;
                    width = widthTest;
                    if (choiceMenu.getValue().equals("Geography")) {
                        gameDocument = "geography.txt";
                        stage.close();
                        mainGUI(gameDocument, stage);
                    } else if (choiceMenu.getValue().equals("All words")) {
                        gameDocument = "english-words.txt";
                        stage.close();
                        mainGUI(gameDocument, stage);
                    }
                }
            }
        });

        //Format the window to look nice
        GridPane options = new GridPane();
        options.setPadding(new Insets(5, 10, 5, 10));
        VBox menu = new VBox();
        Label dropDown = new Label("Choose the types of words to search:");
        Label info = new Label("Enter a width and height (minimum 10, maximum 30):");
        info.setPadding(new Insets(5, 0, 0, 0));
        menu.getChildren().addAll(dropDown, choiceMenu, info);
        menu.setSpacing(6);
        menu.setPadding(new Insets(5, 10, 5, 10));
        HBox heightBox = new HBox();
        heightBox.getChildren().addAll(enteredHeight, hLabel);
        heightBox.setSpacing(6);
        heightBox.setPadding(new Insets(5, 10, 5, 10));
        HBox widthBox = new HBox();
        widthBox.getChildren().addAll(enteredWidth, wLabel);
        widthBox.setSpacing(6);
        widthBox.setPadding(new Insets(5, 10, 5, 10));
        options.setVgap(4);
        options.setHgap(6);
        options.add(menu, 0, 0);
        options.add(heightBox, 0, 1);
        options.add(widthBox, 0, 2);
        pane.setCenter(options);
        grid.add(startButton, 26, 0);
        grid.setPadding(new Insets(5,5,5,5));
        pane.setBottom(grid);
        Scene scene = new Scene(new StackPane(pane));
        stage.setScene(scene);
        stage.getIcons().add(new Image(WordSearchGUI.class.getResourceAsStream("icon.png")));
        stage.setResizable(false);
        stage.show();

    }

    /*
    This only needs to display 2 main things: the board, and the list of words on either one side or the bottom.
    The main functionalities I want are: 1) striking out a word when it's found, 2) highlighting a letter that's
    selected, and 3) circling words that have been found.
    If a tile is the end of a word and it's clicked on, see if where the mouse is released matches the numbers saved
    in that word's Pair at the start of the list.
    If a tile is the end and start of the word, see if where the mouse lands is the corresponding endpoint of the word.
    If a tile is the start or end of two different words, see if where the mouse lands is the starting/ending point
     */

    /**
     * After the user has decided what to search for, this main GUI displays the word search
     * itself. The letters are displayed on the left with the word list displayed on the right;
     * when a user finds a word on the left, it gets crossed out from the word list on the right.
     * When a user starts dragging on a letter that starts or ends a word, that's taken note of
     * and when the user releases, the letter they release on is also taken note of, and if it's
     * the start or end of the same word, then the word is circled and, as mentioned above, is
     * crossed out in the word list.
     * @param s the name of the dictionary to be used.
     * @param stage the background stage.
     */
    public void mainGUI(String s, Stage stage) {
        WSManager manager = new WSManager(s, height, width, solve);
        Rectangle2D screenSize = Screen.getPrimary().getBounds();
        double screenHeight = screenSize.getHeight();
        board = manager.getLetters();
        ArrayList<String> words = manager.getWords();
        GridPane wordList = new GridPane();
        RowConstraints rowCon = new RowConstraints();
        rowCon.setMaxHeight(40);
        wordList.getRowConstraints().add(rowCon);

        int colCounter = 0, rowCounter = 0;
        ArrayList<Text> texts = new ArrayList<>();

        //Set up the word list on the right. Makes sure to put words in different columns
        //if there's enough
        for (String str: words) {
            BorderPane bp = new BorderPane();
            Text t = new Text(str);
            t.setFont(Font.font("Helvetica", 14));
            texts.add(t);
            bp.setLeft(t);
            bp.setStyle("-fx-background-color: white;");
            bp.setPadding(new Insets(5, 5, 5, 5));
            wordList.add(bp, colCounter, rowCounter);
            if (rowCounter + 1 >= height * 4 / 5) {
                colCounter++;
                rowCounter = 0;
                continue;
            }
            rowCounter++;
        }
        wordList.setPadding(new Insets(5, 20, 5, 10));
        wordList.setBorder(new Border(new BorderStroke(Color.GRAY, BorderStrokeStyle.SOLID,
                CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        BorderPane holder = new BorderPane();

        //Fillers for padding on the side, since placing something on the right of a BorderPane
        //always moves its contents as far right as possible, disregarding padding
        BorderPane filler1 = new BorderPane();
        BorderPane filler2 = new BorderPane();
        BorderPane filler3 = new BorderPane();
        filler1.setPadding(new Insets(5, 5, 5, 5));
        filler2.setPadding(new Insets(5, 5, 5, 5));
        filler3.setPadding(new Insets(2,5,2,5));
        holder.setBottom(filler1);
        holder.setRight(filler2);
        holder.setTop(filler3);
        holder.setCenter(wordList);

        BorderPane gpHolder = new BorderPane();
        GridPane gp = new GridPane();
        for (int i = 1; i < height + 1; i++) {
            for (int j = 1; j < width + 1; j++) {
                //Set up each tile with its corresponding letter
                Tile tile = board[i][j];
                BorderPane b = new BorderPane();
                b.setMinSize(screenHeight/WSConstants.SQUARE_SIZE_DIVIDER,
                        screenHeight/WSConstants.SQUARE_SIZE_DIVIDER);
                b.setMaxSize(screenHeight/WSConstants.SQUARE_SIZE_DIVIDER,
                        screenHeight/WSConstants.SQUARE_SIZE_DIVIDER);
                Text t = new Text("" + board[i][j].getLetter());
                t.setFont(Font.font("Helvetica", FontWeight.BOLD, 16));
                b.setStyle("-fx-background-color: white;");
                b.setCenter(t);

                //Drag and drop setup.
                b.setOnDragDetected(event -> {
                        if (tile.getStart()) atTile.addAll(tile.startWords());
                        if (tile.getEnd()) atTile.addAll(tile.endWords());
                        startingTile = tile;
                        Dragboard db = b.startDragAndDrop(TransferMode.ANY);
                        ClipboardContent cb = new ClipboardContent();
                        cb.putString(" ");
                        db.setContent(cb);
                });
                b.setOnDragExited(event -> {
                        endingTile = tile;
                });

                //When the drag is done, check if a word was found. If it was, then strikethrough
                //the word in the word list on the right.
                b.setOnDragDone(event -> {
                    if (tile.isPartOfWord()) {
                        for (Word w : atTile) {
                            if (!endingTile.equals(startingTile)) {
                                if ((endingTile.equals(w.getEndTile()) &&
                                        startingTile.equals(w.getStartTile()) ||
                                        (endingTile.equals(w.getStartTile()) &&
                                                startingTile.equals(w.getEndTile())))) {
                                    for (Text tx : texts) {
                                        if (tx.getText().
                                                replaceAll("[^a-zA-Z]","").
                                                replaceAll("\\s", "").
                                                equals(w.getName())) {
                                            tx.setStyle("-fx-strikethrough: true");
                                            break;
                                        }
                                    }
                                    //Place an ellipse with the center over the middle of the word,
                                    //and rotate it if it's a diagonal word
                                    switch(w.getDirection()) {
                                        case DOWN:
                                        case UP:
                                            Ellipse down = new Ellipse(
                                                    (((w.getStartTile().getX()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)
                                                            + ((w.getEndTile().getX()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER))/2.0
                                                            - (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)/4.0,
                                                    (((w.getStartTile().getY()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)
                                                            + ((w.getEndTile().getY()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER))/2.0
                                                            - (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)/4.0,
                                                    (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)/2.0,
                                                    (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER * w.getName().length())/2.0);
                                            down.setStroke(Color.DIMGRAY);
                                            down.setOpacity(0.75);
                                            down.setStrokeWidth(3);
                                            down.setFill(Color.TRANSPARENT);
                                            down.setMouseTransparent(true);
                                            gpHolder.getChildren().add(down);
                                            break;
                                        case RIGHT:
                                        case LEFT:
                                            Ellipse right = new Ellipse(
                                                    (((w.getStartTile().getX()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)
                                                    + ((w.getEndTile().getX()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER))/2.0
                                                    - (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)/4.0
                                                    ,
                                                    (((w.getStartTile().getY()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)
                                                            + ((w.getEndTile().getY()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER))/2.0
                                                            - (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)/4.0
                                                    ,
                                                    (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER * w.getName().length())/2.0,
                                                    (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)/2.0);
                                            right.setStroke(Color.DIMGRAY);
                                            right.setOpacity(0.75);
                                            right.setStrokeWidth(3);
                                            right.setFill(Color.TRANSPARENT);
                                            right.setMouseTransparent(true);
                                            gpHolder.getChildren().add(right);
                                            break;
                                        case DIAGONAL_DOWN_LEFT:
                                        case DIAGONAL_UP_RIGHT:
                                            Ellipse downLeft = new Ellipse(
                                                    (((w.getStartTile().getX()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)
                                                            + ((w.getEndTile().getX()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER))/2.0
                                                            - (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)/4.0,
                                                    (((w.getStartTile().getY()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)
                                                            + ((w.getEndTile().getY()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER))/2.0
                                                            - (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)/4.0,
                                                    (Math.sqrt(Math.pow(screenHeight/WSConstants.SQUARE_SIZE_DIVIDER, 2) +
                                                            Math.pow(screenHeight/WSConstants.SQUARE_SIZE_DIVIDER, 2))) *
                                                            w.getName().length()/2.0,
                                                    (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)/2.0);
                                            downLeft.setStroke(Color.DIMGRAY);
                                            downLeft.setOpacity(0.75);
                                            downLeft.setStrokeWidth(3);
                                            downLeft.setFill(Color.TRANSPARENT);
                                            Rotate r1 = new Rotate();
                                            r1.setAngle(315);
                                            r1.setPivotX((((w.getStartTile().getX()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)
                                                    + ((w.getEndTile().getX()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER))/2.0
                                                    - (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)/4.0);
                                            r1.setPivotY((((w.getStartTile().getY()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)
                                                    + ((w.getEndTile().getY()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER))/2.0
                                                    - (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)/4.0);
                                            downLeft.getTransforms().add(r1);
                                            downLeft.setMouseTransparent(true);
                                            gpHolder.getChildren().add(downLeft);
                                            break;
                                        case DIAGONAL_DOWN_RIGHT:
                                        case DIAGONAL_UP_LEFT:
                                            Ellipse downRight = new Ellipse(
                                                    (((w.getStartTile().getX()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)
                                                            + ((w.getEndTile().getX()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER))/2.0
                                                            - (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)/4.0
                                                    ,
                                                    (((w.getStartTile().getY()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)
                                                            + ((w.getEndTile().getY()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER))/2.0
                                                            - (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)/4.0
                                                    ,
                                                    (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)/2.0
                                                    ,
                                                    Math.sqrt(Math.pow(screenHeight/WSConstants.SQUARE_SIZE_DIVIDER, 2) +
                                                    Math.pow(screenHeight/WSConstants.SQUARE_SIZE_DIVIDER, 2)) *
                                                    w.getName().length()/2.0);
                                            downRight.setStroke(Color.DIMGRAY);
                                            downRight.setOpacity(0.75);
                                            downRight.setStrokeWidth(3);
                                            downRight.setFill(Color.TRANSPARENT);
                                            Rotate r2 = new Rotate();
                                            r2.setAngle(135);
                                            r2.setPivotX((((w.getStartTile().getX()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)
                                                    + ((w.getEndTile().getX()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER))/2
                                                    - (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)/4.0);
                                            r2.setPivotY((((w.getStartTile().getY()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)
                                                    + ((w.getEndTile().getY()) * screenHeight/WSConstants.SQUARE_SIZE_DIVIDER))/2
                                                    - (screenHeight/WSConstants.SQUARE_SIZE_DIVIDER)/4.0);
                                            downRight.getTransforms().add(r2);
                                            downRight.setMouseTransparent(true);
                                            gpHolder.getChildren().add(downRight);
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    startingTile = null;
                    endingTile = null;
                    atTile.clear();
                });
                gp.add(b, j, i);
            }
        }
        gp.setPadding(new Insets(5, 5, 5, 5));

        //Format the window to look nice
        HBox h = new HBox();
        gpHolder.setCenter(gp);
        h.getChildren().addAll(gpHolder, holder);
        BorderPane pane = new BorderPane();
        pane.setStyle("-fx-background-color: white;");
        pane.setCenter(h);
        //Large ending x to ensure the line always goes across the whole screen
        Line divider = new Line(0, 0, 200000, 0);
        divider.setStrokeWidth(3);
        pane.getChildren().add(divider);
        Scene scene = new Scene(new StackPane(pane));
        stage.setScene(scene);
        stage.setTitle("Word Search");
        stage.show();
    }
}