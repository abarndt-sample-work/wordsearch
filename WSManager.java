/**
 * @author: Andrew Barndt
 * Class description: creates and maintains the words and letters of the word search.
 */

import java.io.*;
import java.util.*;

public class WSManager {

    //Global variables needed
    private int height, width, minNumOfWords;
    private boolean solve;
    private char grid[][];
    private Tile letters[][];
    private String gameType;
    private ArrayList<String> geoDictionary = new ArrayList<>();
    private ArrayList<String> allDictionary = new ArrayList<>();
    private ArrayList<Word> toRemove = new ArrayList<>();
    private HashSet<String> trieFound = new HashSet<>();
    private HashSet<Word> words = new HashSet <>();
    private HashSet<String> originalWords = new HashSet<>();
    private Random rand = new Random();
    private Direction[] dirs = Direction.values();
    private Trie trie = new Trie();

    /**
     * Constructor for the manager. Takes a string to determine what kind of word search
     * to use, and creates a 2D array with a "border" that isn't displayed so as to avoid
     * array out of bounds error checking. It then reads in line by line a dictionary and
     * adds it to the trie that is later used to solve the word search.
     * @param s the type of word search (geographical locations or the full English language)
     * @param h the height of the word search (minimum 10, maximum 30)
     * @param w the width of the word search (minimum 10, maximum 30)
     */
    public WSManager (String s, int h, int w, boolean b) {
        height = h + 2;
        width = w + 2;
        gameType = s;
        solve = b;
        this.minNumOfWords = (h * w)/WSConstants.NUM_LETTERS_PER_WORD;
        if (s.equals("english-words.txt") || s.equals("geography.txt")) {
            try (InputStream in = getClass().getClassLoader().getResourceAsStream(s);
                 Scanner scanner = new Scanner(new BufferedReader(new InputStreamReader(in)))) {
                if (s.equals("english-words.txt")) {
                    while (scanner.hasNextLine()) {
                        String str = scanner.nextLine();
                        allDictionary.add(str);
                        //eliminate spaces and non-letter characters
                        str = str.replaceAll("\\s", "");
                        str = str.replaceAll("[^a-zA-Z]", "");
                        trie.insert(str.toUpperCase());
                    }
                } else if (s.equals("geography.txt")) {
                    while (scanner.hasNextLine() && scanner.hasNext()) {
                        String str = scanner.nextLine();
                        geoDictionary.add(str);
                        str = str.replaceAll("\\s", "");
                        str = str.replaceAll("[^a-zA-Z]", "");
                        trie.insert(str.toUpperCase());
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            setUpSearch();
        }
    }

    /**
     * Sets up the word search. Starts by putting the borders around the edge and denoting
     * the rest as open to put a letter on, then generates the word search and any remaining
     * open spaces. If wanted, can also demonstrate the functionality of the trie by solving
     * the word search and displaying all possible words, though that is commented out
     * at the moment.
     */
    private void setUpSearch() {
        System.out.println();
        this.grid = new char[height][width];
        this.letters = new Tile[height][width];
        for (int i = 0; i < height; i++) grid[i][0] = WSConstants.BORDER;
        for (int i = 0; i < height; i++) grid[i][width - 1] = WSConstants.BORDER;
        for (int i = 0; i < width; i++) grid[0][i] = WSConstants.BORDER;
        for (int i = 0; i < width; i++) grid[height - 1][i] = WSConstants.BORDER;
        for (int i = 1; i < height - 1; i++) {
            for (int j = 1; j < width - 1; j++) {
                grid[i][j] = WSConstants.OPEN;
            }
        }
        words.clear();
        originalWords.clear();
        chooseWords();
        fillRemaining();
        if (solve) {
            trieSolve();
            System.out.println(trieFound);
        }
//        for (Word w: words) {
//            System.out.print(w.getName() + " ");
//        }
//        System.out.println();
    }

    /**
     * Adds a certain number of words to be checked to see if they're able to be put into
     * the word search, by putting in twice as many words to test as the number of remaining
     * words needed to reach the minimum word count for the word search.
     */
    private void chooseWords() {
        while (words.size() < minNumOfWords) {
            if (gameType.equals("geography.txt")) addWords(geoDictionary,
                    words.size() + (minNumOfWords - words.size()) * 2);
            else addWords(allDictionary,
                    words.size() + (minNumOfWords - words.size()) * 2);
            testWords();
        }
    }

    /**
     * Loops through all words that could be added, and randomly chooses a location and a
     * direction for that word. If that location doesn't work, either due to a letter
     * mismatch or running off the edge, try again until the specified number of tries has
     * been reached, at which point the word is removed from consideration. If we reach the
     * end of the word without any problems, the word is placed onto the grid of letters.
     */
    private void testWords() {
        for(Word w: words) {
            if(w.getUsed()) continue;
            boolean wordWorked = false;
            for (int i = 0; i < WSConstants.TRIES_PER_WORD; i++) {
                int counter = 0;
                Direction d = dirs[rand.nextInt(dirs.length)];
                int xStart = 1 + rand.nextInt(width - 2);
                int yStart = 1 + rand.nextInt(height - 2);
                for (int j = 0; j < w.getName().length(); j++) {
                    if (grid[yStart + (d.getyChange() * j)][xStart + (d.getxChange() * j)]
                            == WSConstants.OPEN || grid[yStart + (d.getyChange() * j)][xStart + (d.getxChange() * j)]
                            == w.getName().charAt(j)) {
                        counter++;

                    } else {
                        if (i + 1 == WSConstants.TRIES_PER_WORD) {
                            toRemove.add(w);
                        }
                        break;
                    }
                    if(counter == w.getName().length()) {
                        w.setDirection(d); w.setUsed();
                        wordWorked = true;
                        placeWord(w, xStart, yStart, d);
                    }
                }
                if(wordWorked) break;
            }
        }
        removeWords();
    }

    /**
     * Adds words that will be sent to the testWords method, formatting them accordingly.
     * @param dictionary the list of words to choose from.
     * @param bound the number of words to be made.
     */
    private void addWords (ArrayList<String> dictionary, int bound) {
        do {
            boolean isContained = false;
            int randIndex = rand.nextInt(dictionary.size());
            String str = dictionary.get(randIndex).replaceAll("\\s", "");
            str = str.replaceAll("[^a-zA-Z]", "");
            if (str.length() <= WSConstants.MAX_WORD_LENGTH &&
                    str.length() >= WSConstants.MIN_WORD_LENGTH) {
                for (Word w: words) {
                    if (w.getName().equals(str)) isContained = true;
                }
                if (!isContained) {
                    originalWords.add(dictionary.get(randIndex));
                    words.add(new Word(str));
                }
            }
        } while (words.size() < bound);
    }

    /**
     * After words have been tested and no place for them was found, call this method
     * to remove them from both the list of words to put down and the list of original
     * words.
     */
    private void removeWords() {
        for (Word w: toRemove) {
            Iterator<String> itr = originalWords.iterator();
            while (itr.hasNext()) {
                String s = itr.next();
                String test = s.replaceAll("\\s", "");
                test = test.replaceAll("[^a-zA-Z]", "");
                if(w.getName().equals(test)) {
                    itr.remove();
                    break;
                }
            }
            words.remove(w);
        }
        toRemove.clear();
    }

    /**
     * Places the word down on the grid of letters, after it's been tested and shown it can
     * be placed there.
     * @param word the word being placed down
     * @param xStart the x-coordinate of the start of the word
     * @param yStart the y-coordinate of the start of the word
     * @param dir the direction the word is going
     */
    public void placeWord(Word word, int xStart, int yStart, Direction dir) {
        int counter = 0;
        String s = word.getName().toUpperCase();
        for (int i = 0; i < s.length(); i++) {
            grid[yStart + (dir.getyChange() * counter)]
                    [xStart + (dir.getxChange() * counter)] = s.charAt(counter);

            //If there is no letter in that location, make a new tile and set it accordingly
            if (letters[yStart + (dir.getyChange() * counter)]
                    [xStart + (dir.getxChange() * counter)] == null) {
                Tile t = new Tile(s.charAt(counter), xStart + (dir.getxChange() * counter),
                        yStart + (dir.getyChange() * counter));
                t.setPartOfWord(true); t.addDirOfWord(dir);
                if (counter == 0) {
                    t.setStart(word);
                    word.setStartOfWord(t);
                }
                if (counter == s.length() - 1) {
                    t.setEnd(word);
                    word.setEndOfWord(t);
                }
                letters[yStart + (dir.getyChange() * counter)]
                        [xStart + (dir.getxChange() * counter)] = t;
            }
            //If the word is intersecting with another word, do the same thing except without
            //making a new tile
            else {
                letters[yStart + (dir.getyChange() * counter)]
                        [xStart + (dir.getxChange() * counter)].addDirOfWord(dir);
                if (counter == 0) {
                    letters[yStart + (dir.getyChange() * counter)]
                        [xStart + (dir.getxChange() * counter)].setStart(word);
                    word.setStartOfWord(letters[yStart + (dir.getyChange() * counter)]
                            [xStart + (dir.getxChange() * counter)]);
                }
                if (counter == s.length() - 1) {
                    letters[yStart + (dir.getyChange() * counter)]
                        [xStart + (dir.getxChange() * counter)].setEnd(word);
                    word.setEndOfWord(letters[yStart + (dir.getyChange() * counter)]
                            [xStart + (dir.getxChange() * counter)]);
                }
            }
            counter++;
        }
    }

    /**
     * Once all words have been placed, randomly fill the rest of the word search
     */
    public void fillRemaining() {
        for (int i = 1; i < height - 1; i++) {
            for (int j = 1; j < width - 1; j++) {
                if (grid[i][j] == WSConstants.OPEN) {
                    char c = (char)(rand.nextInt(26) + 'A');
                    grid[i][j] = c;
                    Tile t = new Tile(c, j ,i);
                    letters[i][j] = t;
                }
            }
        }
    }

    /**
     * Getter method for the letter 2D array.
     * @return the grid of characters in the word search.
     */
    public Tile[][] getLetters() {
        return this.letters;
    }

    /**
     * Get the set of original words as a list.
     * @return a list of the set of words as originally written, before things like extra
     * apostrophes were removed
     */
    public ArrayList<String> getWords() {
        ArrayList<String> finalWords = new ArrayList<>();
        finalWords.addAll(originalWords);
        return finalWords;
    }

    /**
     * String representation of the letter grid.
     * @return a 2D representation of the wordsearch area.
     */
    public String toString() {
        String s = "";
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                s += grid[i][j];
            }
            s += "\n";
        }
        return s;
    }

    /**
     * If desired, this method can be called to find every word in the word search by using the
     * trie created when the word search was constructed. From every letter in the search, go in
     * all possible directions until no word begins with the letters going that direction. Add
     * each word that's in the trie to a list of words, and at the end that list of words will be
     * the complete list of words within the word search.
     */
    public void trieSolve() {
        Direction[] dirs = Direction.values();
        for (int i = 1; i < height - 1; i++) {
            for (int j = 1; j < width - 1; j++) {
                String s = "";
                for (int k = 0; k < Direction.values().length; k++) {
                    Direction d = dirs[k];
                    int counter = 0;
                    for (int m = 0; m < WSConstants.MAX_WORD_LENGTH; m++) {
                        if (grid[i + (d.getyChange() * counter)][j + (d.getxChange() * counter)] == WSConstants.BORDER
                        || grid[i + (d.getyChange() * counter)][j + (d.getxChange() * counter)] == WSConstants.OPEN) {
                            s = "";
                            break;
                        }
                        s += grid[i + (d.getyChange() * counter)][j + (d.getxChange() * counter)];
                        counter++;
                        if (trie.search(s) && s.length() > 2) {
                            trieFound.add(s);
                        }
                        if (!trie.startsWith(s)) {
                            s = "";
                            break;
                        }
                        if (m == WSConstants.MAX_WORD_LENGTH - 1) s = "";
                    }
                }
            }
        }
    }
}