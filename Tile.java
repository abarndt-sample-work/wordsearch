/**
 * @author: Andrew Barndt
 * Class description: describes the behavior of an individual letter's space in a word search,
 * such as what letter it is, its x and y coordinates within the overall word search, if words
 * start/end on it and if so which ones, if it's part of a word at all, and what directions those
 * words are going.
 */

import java.util.ArrayList;

public class Tile {

    private char letter;
    private ArrayList<Direction> wordDirs = new ArrayList<>();
    private int x, y;
    private boolean isStart;
    private boolean isEnd;
    private ArrayList<Word> endOnTile = new ArrayList<>();
    private ArrayList<Word> startOnTile = new ArrayList<>();
    private boolean partOfWord;

    /**
     * Constructor for a tile. Sets the letter and the x-y coordinates of the tile.
     * @param c the letter in that space.
     * @param x the x coordinate of that space.
     * @param y the y coordinate of that space.
     */
    public Tile (char c, int x, int y) {
        this.letter = c;
        this.x = x;
        this.y = y;
    }

    /**
     * Adds the direction of a word occupying the tile to the list of directions of words that
     * occupy that tile (since more than one word can use a given tile).
     * @param d the direction to be added.
     */
    public void addDirOfWord(Direction d) {
        wordDirs.add(d);
    }

    /**
     * Denotes that the tile is where at least one word starts, and adds a word that starts there
     * to the list of words that start on that tile.
     * @param w the word that starts on the tile.
     */
    public void setStart(Word w) {
        this.isStart = true;
        startOnTile.add(w);
    }

    /**
     * Denotes that the tile is where at least one word ends, and adds a word that ends there to
     * the list of words that end on that tile.
     * @param w the word that ends on the tile.
     */
    public void setEnd(Word w) {
        this.isEnd = true;
        endOnTile.add(w);
    }

    /**
     * Getter method for the x coordinate of the tile.
     * @return the x coordinate of the tile.
     */
    public int getX() {
        return x;
    }

    /**
     * Getter method for the y coordinate of the tile.
     * @return the y coordinate of the tile.
     */
    public int getY() {
        return y;
    }

    /**
     * Setter method for denoting if a tile is at least part of a word or not.
     * @param b true if the tile is supposed to be part of a word; false otherwise.
     */
    public void setPartOfWord(boolean b) {
        this.partOfWord = b;
    }

    /**
     * Getter method for the letter of the tile.
     * @return the letter of the tile.
     */
    public char getLetter() {
        return letter;
    }

    /**
     * Getter method for finding if the tile has any words starting on it.
     * @return true if at least one word starts on that tile; false otherwise.
     */
    public boolean getStart() {
        return isStart;
    }

    /**
     * Getter method for the list of words that start on the tile.
     * @return the list of words that start on the tile.
     */
    public ArrayList<Word> startWords() {
        return startOnTile;
    }

    /**
     * Getter method for finding if the tile has any words ending on it.
     * @return true it at least one word ends on that tile; false otherwise.
     */
    public boolean getEnd() {
        return isEnd;
    }

    /**
     * Getter method for the list of words that end on the tile.
     * @return the list of words that end on the tile.
     */
    public ArrayList<Word> endWords() {
        return endOnTile;
    }

    /**
     * Getter method for if the tile is part of any words.
     * @return true if the tile has a letter of any word on it.
     */
    public boolean isPartOfWord() {
        return partOfWord;
    }
}