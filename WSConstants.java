/**
 * @author: Andrew Barndt
 * Description: a list of constants used to determine the workings of the word search.
 */

public class WSConstants {

    public static final int MAX_HEIGHT = 30;
    public static final int MAX_WIDTH = 30;
    public static final int MIN_HEIGHT = 10;
    public static final int MIN_WIDTH = 10;
    public static final char OPEN = '.';
    public static final char BORDER = '#';

    //We'll limit ourselves to words no longer than 10 letters and no shorter than 3
    public static final int MAX_WORD_LENGTH = 10;
    public static final int MIN_WORD_LENGTH = 3;

    //The higher the number, the fewer words appear in the search
    public static final int NUM_LETTERS_PER_WORD = 9;

    //Number of times the testWords method tests a word before it's removed from consideration
    public static final int TRIES_PER_WORD = 15;
    public static final int SQUARE_SIZE_DIVIDER = 35;
}
