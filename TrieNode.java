/**
 * @author: I, Andrew Barndt, have revised this significantly, but ultimately credit is due to
 * https://www.programcreek.com/2014/05/leetcode-implement-trie-prefix-tree-java/ for the trie
 * structure. In this class, a node is initialized with 26 sub-nodes (for each letter of the
 * alphabet), and a field denoting if the node marks that a given prefix is a word.
 */

public class TrieNode {
    private TrieNode[] arr;
    private boolean isEnd;

    public TrieNode() {
        this.arr = new TrieNode[26];
    }

    public TrieNode[] getArr() {
        return arr;
    }

    public boolean isEnd() {
        return isEnd;
    }

    public void setEnd(boolean end) {
        isEnd = end;
    }
}
