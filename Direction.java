/**
 * @author: Andrew Barndt
 * Class description: an enumeration of all possible directions in a word search, with their
 * respective changes in x and y position one move in that direction is
 */

public enum Direction {
    UP(0,-1),
    DOWN(0,1),
    RIGHT(1,0),
    LEFT(-1,0),
    DIAGONAL_UP_RIGHT(1,-1), //Northeast
    DIAGONAL_DOWN_RIGHT(1,1), //Southeast
    DIAGONAL_UP_LEFT(-1,-1), //Northwest
    DIAGONAL_DOWN_LEFT(-1,1); //Southwest

    private final int xChange, yChange;

    Direction(int x, int y) {
        xChange = x;
        yChange = y;
    }
    public int getxChange() {
        return this.xChange;
    }

    public int getyChange() {
        return this.yChange;
    }
}
