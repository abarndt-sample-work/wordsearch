# Wordsearch
## Overview
I spent some time this past winter break getting back into Java and especially JavaFX, which I'd be using in my Large Program Design class that I'm currently taking (spring 2020). This project involves reading in text files, generating a word search and possibly solving it within a few seconds at most, and displaying a straightforward and intuitive interface. The entire project was done within IntelliJ IDEA, with some command line testing in a different directory to ensure the .jar worked properly.
## Getting Started
The program can simply run on its own as the executable .jar in the Runnables repository by simply downloading and running it. Alternatively, it can be run after download via the command line, which is necessary if the user wishes to provide command line arguments. The only command line argument the program accepts is if "solve" is the first argument provided, which will then display the list of all words in the word search (more details below). I've also included a couple screenshots of the functioning program at the end of this ReadMe.
## Dictionary
Every word search has some sort of focus. This version has two main ones: the entire English dictionary, or world geography. Adding any others would take just a couple minutes, to add another file along with adding to the manager the ability to read in that file, and add the appropriate options in the display. As the files are read in, the program does three things: 1) it removes non-letter characters (such as spaces and apostrophes), 2) it puts each word into a very long list of words, and 3) it creates a retrieval tree (trie) data structure, which is used in the solving of the wordsearch later on.
## Generation
The algorithm for generating the word search is as follows:
* Step 1: Determine how many words the word search at a minimum needs (currently set for one word for every 9 letters, so for a 10 x 10 word search with 100 letters, there would be no fewer than 100/9 = 11 words).
* Step 2: At random, choose twice the number of words (we may end up with more than the minimum number of words, which just makes it even more fun) as necessary from the dictionary and put them into a new list. Make sure the words are neither too long nor too short for the word search - currently, no words longer than 10 letters or shorter than 3 letters are permitted.
* Step 3: For each of the words in the new list, select a random location within the word search and a random direction and test if the word can be put there. If it encounters the edge of the 2D array (the 2D array is two elements larger than the word search size, to provide a buffer on each side and avoid out of bounds errors) or encounters an already-placed letter that doesn't match the letter of the next word, try another random location and direction. Repeat until either a viable location and direction are found, or the specified number of times to repeat the process (currently 15 times) is reached.
* Step 4: When all words in this new list are processed, see how many words, if any, are still needed to reach the minimum number. Take that number, if larger than 0, and go back to step 1.
* Step 5: If the minimum number of words is reached, fill in the rest of the word search with randomly chosen letters.

This process is fairly quick (although I haven't sat down and analyzed its time complexity), and is barely noticeable at worst when setting it up. The random placement and directions means that words overlap less than most word searches I've seen and often the words end up goind the same direction, but I wasn't striving to build the perfect word search every time so much as a solid one that I could return to and optimize, while making sure I completed the project before winter break finished.
## Solving
After the word search is fully generated, there is an option, if so desired, to automatically find every word in the word search (even if not explicitly listed as being in the word search). Simply type "solve" as a command line argument, and when you hit "start" to generate the search, a list of all words as they appear in the word search itself (all capitals; spaces and special characters removed) will print in the terminal. The way the algorithm works is as follows:
* Step 1: start at a given letter in the word search (going from top left to bottom right in this project).
* Step 2: Add letters to the word going in a given direction, checking at each step if 1) the current string is a word, and 2) if any words start (but aren't completed) with the current string of letters. If 1) is true, then add it to the list of found words. If 2) is true, then there is no need to go in the same direction any further and the algorithm moves on to the next direction.
* Step 3: Repeat step 2 until all directions have been exhausted.
* Step 4: Move to another letter and repeat steps 1-3 until all letters have been a starting point.

## Display
### Opening Window
This is the window that has the user enter the specifics about the kind of word search they want. From a drop-down menu they can select the type of word search (as mentioned before, either all English words or geography) and how large the search is. Key features:
* When the user tries to hit start but has not selected a type of word search, a dialog window pops up telling them to choose a type of word search.
* When the user has inputted an invalid value into the height or width, a red label appears next to the box indicating to them that the value is invalid.
* When the user tries to hit start with invalid height or width values, nothing happens.

### Word Search Window
After the user enters valid parameters for the word search, the JavaFX stage is closes, the word search is generated, and the stage is re-shown with the word search on the left and the words to search for on the right. I've found this to be a pretty understandable and easy to use interface for the user. Main features:
* Drag and release system: click and drag on a letter. If the letter is the start of a word, and the user drags and releases on the letter that is the end of that word (or the user starts on the ending letter and releases on the start), then the word is found and an ellipse is placed around it.
* When a word is found, the corresponding word on the right side is struck out to indicate it's been found.

## What It Looks Like
![Opening window](https://i.imgur.com/nS2dIFy.png)
![Main window](https://i.imgur.com/ZT47kVJ.png)
