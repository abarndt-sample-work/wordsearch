/**
 * @author: I, Andrew Barndt, have revised this a bit, but credit should go to
 * https://www.programcreek.com/2014/05/leetcode-implement-trie-prefix-tree-java/ for the actual
 * implementation. I've adjusted this code and the TrieNode class to have more restricted access,
 * as well as cleaning up some of the spacing and formatting the page used to fit with how the
 * rest of this project is written.
 */

public class Trie {
    private TrieNode root;

    /**
     * Constructs the trie with just one initial node to start. More nodes are added as words are
     * added.
     */
    public Trie() {
        root = new TrieNode();
    }

    /**
     * Inserts a word into the trie by starting at the root and working down the nodes within. If a
     * node where that letter would be is null, a new node is created. When the word is finished
     * being added, the last note visited is marked as an ending node.
     * @param word the word being added to the trie.
     */
    public void insert(String word) {
        TrieNode p = root;
        for(int i = 0; i < word.length(); i++){
            char c = word.charAt(i);
            int index = c - 'A';
            if (p.getArr()[index] == null){
                TrieNode temp = new TrieNode();
                p.getArr()[index] = temp;
                p = temp;
            } else p = p.getArr()[index];
        }
        p.setEnd(true);
    }

    /**
     * Searches to see if a word is within the trie. If a word isn't in the trie, it's not a valid
     * word that the solver found.
     * @param word the word being searched for.
     * @return true if the word is in the trie; false if not
     */
    public boolean search(String word) {
        TrieNode p = searchNode(word);
        if (p == null) return false;
        else if (p.isEnd()) return true;
        return false;
    }

    /**
     * Tests if the given string, even if it isn't a word itself, has at least one word that starts
     * with that sequence of letters.
     * @param prefix the sequence of letters that may or may not be the beginning of a word(s).
     * @return true if the prefix is the beginning of a word; false if not.
     */
    public boolean startsWith(String prefix) {
        TrieNode p = searchNode(prefix);
        if (p == null) return false;
        else return true;
    }

    /**
     * Searches the trie, looking for a given string. Traverses one level deeper for each
     * character until either the end of the string is reached or a null node is encountered.
     * @param s the word being searched for.
     * @return the node the search for the word stops at. Null if the string is not within the
     * trie, and a regular trie node if the string is within the trie.
     */
    public TrieNode searchNode(String s){
        TrieNode p = root;
        for(int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int index = c - 'A';
            if (p.getArr()[index] != null){
                p = p.getArr()[index];
            } else {
                return null;
            }
        }
        if (p == root) {
            return null;
        }
        return p;
    }
}
